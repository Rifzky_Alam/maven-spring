package com.zeke.beans;

import java.security.PublicKey;

public class MahasiswaBean {
	
	private String npm;
	private String nama;
	private String kelas;
	
	public MahasiswaBean() {
		
	}
	
	public MahasiswaBean(String npm, String nama, String kelas) {
		this.npm = npm;
		this.nama = nama;
		this.kelas = kelas;
	}
	
	
	public String getNpm() {
		return npm;
	}
	public void setNpm(String npm) {
		this.npm = npm;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getKelas() {
		return kelas;
	}
	public void setKelas(String kelas) {
		this.kelas = kelas;
	}
	
	
}
