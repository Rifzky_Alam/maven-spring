package com.zeke.database.mysql;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import com.zeke.beans.MahasiswaBean;


public class Facinstitute {
	protected String JDBC_Driver = "com.mysql.jdbc.Driver";
    protected String DB_URL="jdbc:mysql://localhost:3306/zeke";
    protected String user = "root";
    protected Connection conn;
    protected Statement stmt;
	
	public void Connect() {
		try {
            Class.forName(JDBC_Driver).newInstance();
            conn = DriverManager.getConnection(DB_URL,"root","");
                    
        } catch (Exception e) {
            
        }
	}
	
	public ArrayList<MahasiswaBean> getMhs() {
		ArrayList<MahasiswaBean> hasil = new ArrayList<MahasiswaBean>();
		try {
			this.Connect();
			stmt = conn.createStatement();
			String query;
			query = "SELECT * FROM mahasiswa";
			ResultSet rs = stmt.executeQuery(query);
			
			while(rs.next()) {
				hasil.add(new MahasiswaBean(rs.getString("npm"), rs.getString("nama"), rs.getString("kelas")));
				System.out.println(rs.getString("npm")+ " "+ rs.getString("nama")+" "+ rs.getString("kelas"));	
			}
		} catch (Exception e) {
			System.out.println(e.toString());
			hasil.add(new MahasiswaBean("exception", e.toString(), "exception"));
		}
		return hasil;
	}
	
	public boolean InsertNewData(MahasiswaBean mhs) {
		try {
			this.Connect();
			
			String query;
			query = "INSERT INTO mahasiswa (npm, nama, kelas) VALUES(?,?,?)";
			PreparedStatement stmnt = conn.prepareStatement(query);
			stmnt.setString(1, mhs.getNpm());
			stmnt.setString(2, mhs.getNama());
			stmnt.setString(3, mhs.getKelas());
			stmnt.executeUpdate();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
}
