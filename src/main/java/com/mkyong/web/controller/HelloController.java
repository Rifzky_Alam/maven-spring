package com.mkyong.web.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.zeke.beans.MahasiswaBean;
import com.zeke.database.mysql.Facinstitute;
import com.zeke.files.FileModel;

@Controller
public class HelloController {
	@Autowired
	ServletContext context;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		
		model.addAttribute("message", "Spring 3 MVC Hello World");
		return "hello";

	}

	@RequestMapping(value = "/hello/{name:.+}", method = RequestMethod.GET) //it's fine to write {name} only instead of {name:.+}
	public ModelAndView hello(@PathVariable("name") String name) {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("hello");
		model.addObject("msg", name);

		return model;

	}
	
	@RequestMapping(value = "/rifzky", method = RequestMethod.GET)
	public ModelAndView zeke() {
		ArrayList<MahasiswaBean> bean = new ArrayList<MahasiswaBean>();
		Facinstitute fac = new Facinstitute();
		bean = fac.getMhs();
		ModelAndView model = new ModelAndView();
		model.setViewName("zeketestview");
		model.addObject("sizearraylist", bean.get(0).getNama());
		model.addObject("arr", bean);
		return model;
	}
	
	@RequestMapping(value = "/tesinputform", method = RequestMethod.GET)
	public ModelAndView zeketestinputform() {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("inputform");
		return model;
	}
	@RequestMapping(value = "/tespost", method = RequestMethod.POST)
	public ModelAndView zekeprocessinput(@ModelAttribute("mkyong") MahasiswaBean mhs) {
		ModelAndView model = new ModelAndView();
		Facinstitute fac = new Facinstitute();
		if (fac.InsertNewData(mhs)) {
			return new ModelAndView("redirect:https://fac-institute.com");
		} else {
			model.setViewName("error/error");
			return model;
		}
//		model.setViewName("hasilpost");
//		return new ModelAndView("redirect:https://fac-institute.com"); // the redirect link to external link should not have a space between redirect: and the hyperlink
//		return model;
	}
	
	@RequestMapping(value="postupload", method = RequestMethod.POST)
	public ModelAndView postupload(@RequestParam("file") MultipartFile file) throws IOException {
		ModelAndView model = new ModelAndView();
		if (file.isEmpty()) {
			System.out.println("File is empty!");
			model.setViewName("uploadfileform");
			return model;
		} else {
			System.out.println("Fetching File..");
			byte[] bytes = file.getBytes();
			System.out.println("Getting Bytes: Success!");
			String uploadedFolder = context.getRealPath("") + File.separator + "temp" + File.separator;

//			String uploadedFolder = "D://zeke//";
			Path path = Paths.get(uploadedFolder + file.getOriginalFilename());
			System.out.println("Getting Paths: Success!");
			Files.write(path, bytes);
			System.out.println("Writing File: Success!");
			model.addObject("filename", file.getOriginalFilename());//fileName);
			model.addObject("uploadPath", uploadedFolder);//fileName);
			model.setViewName("uploadsuccess");
			return model;
		}
		
	}
	
	@RequestMapping(value="/uploadform",method = RequestMethod.GET)
	public ModelAndView testupload() {
		ModelAndView model = new ModelAndView();
		model.setViewName("uploadfileform");
		return model;
	}
	
	@RequestMapping(value="/redir", method = RequestMethod.GET)
	public ModelAndView ProcessForm() {
		return new ModelAndView("redirect:https://fac-institute.com");
	}
}