<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
	<title>Success Uploading File</title>
</head>
<body>
	The ${filename} has been uploaded onto folder ${uploadPath}
</body>
</html>