<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Spring Input Form Test</title>
 
<spring:url value="/resources/core/css/hello.css" var="coreCss" /> <!-- defining variable url here -->
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<!-- using defined variable above here -->
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
</head>
 
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">Spring 3 MVC Project</a>
	</div>
  </div>
</nav>
 
<div class="jumbotron">
  <div class="container">
	<h1>${title}</h1>
	<p>
	
    </p>
    <p>
		<a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
	</p>
	</div>
</div>
 
<div class="container">
 
  <div class="row">
  	<div class="col-md-12">
  		<form action="/postupload" enctype="multipart/form-data" method="POST">
  			<div class="form-group">
  				<label>File</label>
  				<input type="file" name="file">
  			</div>
  			
  			<button class="btn btn-lg btn-primary">Submit</button>
  		</form>
  	</div>
  </div>
 
 
  <hr>
  <footer>
	<p>Rifzky Alam 2018</p>
  </footer>
</div>
 
<spring:url value="/resources/core/css/hello.js" var="coreJs" />
<spring:url value="/resources/core/css/bootstrap.min.js" var="bootstrapJs" />
 
<script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
 
</body>
</html>