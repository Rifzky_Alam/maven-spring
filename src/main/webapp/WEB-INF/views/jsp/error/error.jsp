<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Database Error Page</title>
 
<spring:url value="/resources/core/css/hello.css" var="coreCss" /> <!-- defining variable url here -->
<spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
<!-- using defined variable above here -->
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
</head>
 
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">Error Page</a>
	</div>
  </div>
</nav>
 
<div class="container">
 
  <div class="row">
  	<div class="col-md-12">
  		<section>
        <h3>Oops, something may happen</h3>
        <p>Sorry we may experienced new problem accessing database right now.</p>  
      </section>
  	</div>
  </div>
 
 
  <hr>
  <footer>
	<p>Rifzky Alam 2018</p>
  </footer>
</div>
 
<spring:url value="/resources/core/css/hello.js" var="coreJs" />
<spring:url value="/resources/core/css/bootstrap.min.js" var="bootstrapJs" />
 
<script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
 
</body>
</html>